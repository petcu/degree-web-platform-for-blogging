package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.logging.Log;
import org.acme.dto.mapper.PageMapper;
import org.acme.model.Author;
import org.acme.model.Page;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class PageRepository implements PanacheRepository<Page> {

    @ConfigProperty(name = "meilisearch.index.page")
    String indexName;

    @Inject
    MeiliSearchRepository meiliSearchRepository;

    @Inject
    PageMapper pageMapper;

    public Page findByUUID(String uuid) {
        return find("uuid = ?1", uuid).firstResult();
    }

    public boolean existByUUID(String uuid) {
        if (uuid == null)
            return false;
        return !Objects.isNull(findByUUID(uuid));
    }

    public boolean deleteByUUID(String uuid) {
        try {
            Page page = findByUUID(uuid);
            page.setAuthors(new HashSet<>());
            save(page);
            meiliSearchRepository.delete(indexName, uuid);
            delete(page);
            return true;
        } catch (Exception e) {
            Log.error(e);
            return false;
        }
    }

    public void save(Page page) {
        if (!existByUUID(page.getUuid()))
            persist(page);
        else
            getEntityManager().merge(page);
        String json = pageMapper.toJSON(page);
        meiliSearchRepository.update(indexName, json);
    }

    public void removeAuthorReference(Author author) {
        List<Page> pages = listAll();
        for (Page page : pages)
            if (page.getAuthors().stream().anyMatch(a -> a.getId().equals(author.getId()))) {
                page.getAuthors().remove(author);
                save(page);
            }
    }
}
