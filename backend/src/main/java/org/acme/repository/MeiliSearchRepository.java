package org.acme.repository;

import com.meilisearch.sdk.*;
import com.meilisearch.sdk.model.SearchResult;
import io.quarkus.logging.Log;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

@ApplicationScoped
public class MeiliSearchRepository {

    private final Client client;

    public MeiliSearchRepository(@ConfigProperty(name = "meilisearch.url") String url, @ConfigProperty(name = "meilisearch.masterkey") String key) {
        Config config = new Config(url, key);
        client = new Client(config);
    }

    public void update(String indexName, String data) {
        try {
            Index index = client.index(indexName);
            index.addDocuments(data);
        } catch (Exception e) {
            Log.error(e);
        }
    }

    public void delete(String indexName, String documentId) {
        try {
            client.index(indexName).deleteDocument(documentId);
        } catch (Exception e) {
            Log.error(e);
        }
    }

    public Response search(String indexName, String query) {
        try {
            SearchResult result = client.index(indexName).search(query);
            return Response.ok(result.getHits()).build();
        } catch (Exception e) {
            Log.error(e);
            return Response.status(404).build();
        }
    }
}
