package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.logging.Log;
import org.acme.dto.author.PasswordDTO;
import org.acme.dto.mapper.AuthorMapper;
import org.acme.model.Author;
import org.acme.model.form.FormAuth;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Objects;

@ApplicationScoped
public class AuthorRepository implements PanacheRepository<Author> {

    @ConfigProperty(name = "meilisearch.index.author")
    String indexName;

    @Inject
    MeiliSearchRepository meiliSearchRepository;

    @Inject
    AuthorMapper authorMapper;

    @Inject
    PageRepository pageRepository;

    public Author findByUsername(String username) {
        return find("username = ?1", username).firstResult();
    }

    public boolean existById(Long id) {
        if (id == null)
            return false;
        return !Objects.isNull(findById(id));
    }

    public boolean existByUsername(String username) {
        if (username == null)
            return false;
        return !Objects.isNull(findByUsername(username));
    }

    public void save(Author author) {
        if (!existById(author.getId()))
            persist(author);
        else
            getEntityManager().merge(author);
        String json = authorMapper.toJSON(author);
        Log.error(json);
        meiliSearchRepository.update(indexName, json);
    }

    public boolean validateLogin(FormAuth auth) {
        Author author = findByUsername(auth.getUsername());
        if (author == null)
            return false;
        return auth.getPassword().equals(author.getPassword());
    }

    public boolean cheangePassword(PasswordDTO dto) {
        Author author = findById(dto.getId());
        try {
            if (author.getPassword().equals(dto.getPassword())) {
                author.setPassword(dto.getNew_password());
                save(author);
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.error(e);
            return false;
        }
    }

    public boolean deleteById(Long id) {
        try {
            Author author = findById(id);
            pageRepository.removeAuthorReference(findById(id));
            meiliSearchRepository.delete(indexName, id.toString());
            delete(author);
            return true;
        } catch (Exception e) {
            Log.error(e);
            return false;
        }
    }
}
