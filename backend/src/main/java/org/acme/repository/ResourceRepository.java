package org.acme.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.logging.Log;
import org.acme.dto.mapper.ResourceMapper;
import org.acme.model.form.FormFileUpload;
import org.acme.model.Resource;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Objects;

@ApplicationScoped
public class ResourceRepository implements PanacheRepository<Resource> {

    @ConfigProperty(name = "meilisearch.index.resource")
    String indexName;

    @Inject
    MeiliSearchRepository meiliSearchRepository;

    @Inject
    ResourceMapper resourceMapper;

    public Resource findByUUID(String uuid) {
        return find("uuid = ?1", uuid).firstResult();
    }

    public boolean existByUUID(String uuid) {
        if (uuid == null)
            return false;
        return !Objects.isNull(findByUUID(uuid));
    }

    public boolean deleteByUUID(String uuid) {
        try {
            Resource resource = findByUUID(uuid);
            meiliSearchRepository.delete(indexName, uuid);
            delete(resource);
            return true;
        } catch(Exception e) {
            Log.error(e);
            return false;
        }
    }

    public Resource save(FormFileUpload form) {
        try {
            Resource resource = resourceMapper.toEntity(form);
            String json = resourceMapper.toJSON(resource);
            meiliSearchRepository.update(indexName, json);
            persist(resource);
            return resource;
        } catch (Exception e) {
            Log.error(e);
            return null;
        }
    }
}
