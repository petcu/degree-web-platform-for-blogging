package org.acme.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;
import org.acme.dto.author.AuthorCreateDTO;
import org.acme.dto.author.AuthorDTO;
import org.acme.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AuthorMapper {

    @Mapping(target = "uuid", source = "author.id")
    @Mapping(target = "date", source = "author.date_created", dateFormat = "yyyy-MM-dd HH:mm")
    AuthorDTO toDTO(Author author);

    @Mapping(target = "date_created", ignore = true)
    Author toEntity(AuthorCreateDTO dto);

    default String toJSON(Author author) {
        return toJSON(toDTO(author));
    }

    default String toJSON(AuthorDTO authorDTO) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(authorDTO);
        } catch(Exception e) {
            Log.error(e.getMessage());
            return "";
        }
    }
}
