package org.acme.dto.mapper;

import org.acme.dto.author.PasswordDTO;
import org.acme.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PasswordMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "password", source = "new_password")
    Author toAuthor(PasswordDTO passwordDTO);
}
