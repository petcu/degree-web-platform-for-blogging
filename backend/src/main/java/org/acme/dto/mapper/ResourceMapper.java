package org.acme.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;
import org.acme.dto.resource.ResourceDTO;
import org.acme.model.Resource;
import org.acme.model.form.FormFileUpload;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ResourceMapper {

    @Mapping(target = "uuid", source = "resource.uuid")
    @Mapping(target = "objectKey", expression = "java(resource.getObjectKey())")
    @Mapping(target = "type", source = "resource.filetype")
    @Mapping(target = "size", source = "resource.filesize")
    @Mapping(target = "date", source = "resource.date_created", dateFormat = "yyyy-MM-dd HH:mm")
    ResourceDTO toDTO(Resource resource);

    @Mapping(target = "filename", expression = "java(form.getFileName())")
    @Mapping(target = "filetype", expression = "java(form.getFileType())")
    @Mapping(target = "filesize", expression = "java(form.getFileSize())")
    Resource toEntity(FormFileUpload form);

    default String toJSON(Resource resource) {
        return toJSON(toDTO(resource));
    }

    default String toJSON(ResourceDTO dto) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(dto);
        } catch (Exception e) {
            Log.error(e.getMessage());
            return "";
        }
    }
}
