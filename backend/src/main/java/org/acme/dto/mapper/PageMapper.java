package org.acme.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;
import org.acme.dto.page.PageCreateDTO;
import org.acme.dto.page.PageDTO;
import org.acme.model.Author;
import org.acme.model.Page;
import org.acme.repository.AuthorRepository;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.*;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PageMapper {

    default PageDTO toDTO(Page page) {
        AuthorMapper authorMapper = new AuthorMapperImpl();
        PageDTO dto = new PageDTO();

        dto.setUuid(page.getUuid());
        dto.setTitle(page.getTitle());
        if (page.getDate_update() != null)
            dto.setDate(page.getDate_update().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        else {
            dto.setDate("");
        }
        if (page.getAuthors() != null)
            dto.setAuthors(page.getAuthors().stream().map(authorMapper::toDTO).collect(Collectors.toList()));
        if (page.getTags() != null)
            dto.setTags(page.getTags().split(","));
        dto.setPublish(page.getPublish());
        dto.setContent(page.getContent());
        return dto;
    }

    default Page toEntity(PageCreateDTO dto) {
        Page page = new Page();
        AuthorRepository authorRepository = new AuthorRepository();

        if (dto.getUuid() != null && dto.getUuid().length() > 0)
            page.setUuid(dto.getUuid());
        if (dto.getTitle() != null)
            page.setTitle(dto.getTitle());
        if (dto.getAuthors() != null) {
            Set<Author> authors = Arrays.stream(dto.getAuthors()).map(authorRepository::findById).collect(Collectors.toSet());
            page.setAuthors(authors);
        }
        if (dto.getTags() != null)
            page.setTags(String.join(",", dto.getTags()));
        if (dto.getPublish() != null)
            page.setPublish(dto.getPublish());
        if (dto.getContent() != null)
            page.setContent(dto.getContent());
        return page;
    }

    default String toJSON(Page page) {
        return toJSON(toDTO(page));
    }

    default String toJSON(PageDTO dto) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(dto);
        } catch (Exception e) {
            Log.error(e.getMessage());
            return "";
        }
    }
}
