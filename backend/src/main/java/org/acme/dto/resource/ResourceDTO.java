package org.acme.dto.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceDTO {
    private String uuid;
    private String objectKey;
    private String type;
    private String size;
    private String date;
}
