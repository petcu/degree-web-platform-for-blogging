package org.acme.dto.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acme.model.Role;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDTO {
    private Long uuid;
    private String username;
    private Set<Role> roles;
    private String name;
    private String date;
    private String about;
}
