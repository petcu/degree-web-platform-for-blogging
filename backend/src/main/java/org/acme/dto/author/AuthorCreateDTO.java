package org.acme.dto.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acme.model.Role;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorCreateDTO {
    private Long id;
    @NotBlank
    private String username;
    private String password;
    private Set<Role> roles;
    private String name;
    private String about;
}
