package org.acme.dto.author;

import io.quarkus.arc.All;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@All
public class PasswordDTO {
    @NotNull
    private Long id;

    @NotBlank
    private String password;

    @NotBlank
    private String new_password;
}
