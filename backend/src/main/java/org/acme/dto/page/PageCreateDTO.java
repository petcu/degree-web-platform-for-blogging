package org.acme.dto.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageCreateDTO {
    private String uuid;
    @NotBlank
    private String title;
    private String date;
    @NotEmpty
    private Long[] authors;
    private String[] tags;
    private Boolean publish;
    private String content;
}
