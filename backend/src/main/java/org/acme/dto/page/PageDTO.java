package org.acme.dto.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.acme.dto.author.AuthorDTO;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageDTO {
    private String uuid;
    private String title;
    private String date;
    private List<AuthorDTO> authors;
    private String[] tags;
    private Boolean publish;
    private String content;
}
