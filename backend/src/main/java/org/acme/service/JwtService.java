package org.acme.service;

import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;
import java.util.Set;

@Singleton
public class JwtService {

    @ConfigProperty(name = "mp.jwt.verify.issuer")
    String issuer;

    @ConfigProperty(name = "jwt.custom.duration")
    Integer duration;

    public String generateJWT(String username, Set<String> roles) {
        return Jwt.issuer(issuer)
                .subject(username)
                .groups(roles)
                .expiresAt(System.currentTimeMillis() + duration)
                .sign();
    }
}
