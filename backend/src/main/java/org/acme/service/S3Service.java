package org.acme.service;

import org.acme.dto.mapper.ResourceMapper;
import org.acme.model.Resource;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.File;

@ApplicationScoped
public class S3Service {

    @Inject
    S3Client s3;

    @Inject
    ResourceMapper resourceMapper;

    @ConfigProperty(name = "bucket.name")
    String bucketName;

    private PutObjectRequest buildPutRequest(Resource resource) {
        return PutObjectRequest.builder()
                .bucket(bucketName)
                .key(resource.getObjectKey())
                .contentType(resource.getFiletype())
                .build();
    }

    private GetObjectRequest buildGetRequest(String objectKey) {
        return GetObjectRequest.builder()
                .bucket(bucketName)
                .key(objectKey)
                .build();
    }

    private DeleteObjectRequest buildDeleteRequest(String objectKey) {
        return DeleteObjectRequest.builder()
                .bucket(bucketName)
                .key(objectKey)
                .build();
    }

    public Response uploadFile(Resource resource, File file) {
        PutObjectResponse putResponse = s3.putObject(buildPutRequest(resource), RequestBody.fromFile(file));
        if (putResponse != null)
            return Response.ok(resourceMapper.toDTO(resource)).build();
        else
            return Response.serverError().build();
    }

    public Response downloadFile(String objectKey) {
        ResponseBytes<GetObjectResponse> objectBytes = s3.getObjectAsBytes(buildGetRequest(objectKey));
        if(objectBytes != null) {
            Response.ResponseBuilder response = Response.ok(objectBytes.asByteArray());
            response.header("Content-Type", objectBytes.response().contentType());
            return response.build();
        } else
            return Response.serverError().build();
    }

    public void deleteFile(String objectKey) {
        DeleteObjectResponse response = s3.deleteObject(buildDeleteRequest(objectKey));
    }
}
