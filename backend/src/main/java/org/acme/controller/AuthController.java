package org.acme.controller;

import org.acme.model.Author;
import org.acme.model.form.FormAuth;
import org.acme.repository.AuthorRepository;
import org.acme.service.JwtService;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/api/v1/auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Auth APIs", description = "Authentication REST APIs")
@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.APIKEY,
        bearerFormat = "JWT"
)
public class AuthController {
    @Inject
    JwtService jwtService;

    @Inject
    AuthorRepository authorRepository;

    @POST
    @Path("")
    @PermitAll
    public Response getJWT(@Valid FormAuth auth) {
        boolean result = authorRepository.validateLogin(auth);
        if (!result) // Unauthorized
            return Response.status(401).build();
        else {
            Author author = authorRepository.findByUsername(auth.getUsername());
            Set<String> roles = author.getRoles().stream().map(String::valueOf).collect(Collectors.toSet());
            return Response.ok(jwtService.generateJWT(author.getUsername(), roles)).build();
        }
    }
}
