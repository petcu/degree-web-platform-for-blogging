package org.acme.controller;

import io.quarkus.logging.Log;
import org.acme.dto.author.AuthorCreateDTO;
import org.acme.dto.author.AuthorDTO;
import org.acme.dto.author.PasswordDTO;
import org.acme.dto.mapper.AuthorMapper;
import org.acme.model.Author;
import org.acme.repository.AuthorRepository;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/v1/authors")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Author APIs", description = "User Management REST APIs")
@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.APIKEY,
        bearerFormat = "JWT"
)
public class AuthorController {

    @Inject
    AuthorRepository authorRepository;

    @Inject
    AuthorMapper authorMapper;

    @GET
    @Path("{id}")
    @Operation(
            operationId = "findById",
            summary = "Get an Author",
            description = "Get an author info from it's user id"
    )
    @PermitAll
    public Response findById(@PathParam("id") Long id) {
        if (!authorRepository.existById(id)) // Not Found
            return Response.status(404).build();
        AuthorDTO dto = authorMapper.toDTO(authorRepository.findById(id));
        return Response.ok(dto).build();
    }

    @GET
    @Path("username/{username}")
    @Operation(
            operationId = "findByUsername",
            summary = "Get an Author",
            description = "Get an author info from it's username"
    )
    @PermitAll
    public Response findByUsername(@PathParam("username") String username) {
        if(!authorRepository.existByUsername(username)) // Not Found
            return Response.status(404).build();
        AuthorDTO dto = authorMapper.toDTO(authorRepository.findByUsername(username));
        return Response.ok(dto).build();
    }

    @GET
    @Path("")
    @Operation(
            operationId = "findAll",
            summary = "List all Authors",
            description = "List all authors with support for pagination"
    )
    @PermitAll
    public List<AuthorDTO> findAll(
            @DefaultValue("0") @QueryParam("page") Integer page,
            @DefaultValue("25") @QueryParam("size") Integer size
    ) {
        return authorRepository.findAll()
                .page(page, size)
                .list()
                .stream()
                .map(authorMapper::toDTO)
                .collect(Collectors.toList());
    }

    @POST
    @Path("")
    @Transactional
    @Operation(
            operationId = "create",
            summary = "Create or Update an author",
            description = "Create or update an user"
    )
//    @RolesAllowed({"WRITER", "ADMIN"})
    @PermitAll()
    public Response create(@Valid AuthorCreateDTO dto) {
        try {
            Author author = authorMapper.toEntity(dto);
            authorRepository.save(author);
            return Response.ok(authorMapper.toDTO(author)).build();
        } catch (Exception e) { // Internal Server Error
            Log.error(e);
            return Response.status(500).build();
        }
    }

    @POST
    @Path("password")
    @Transactional
    @Operation(
            operationId = "changePassword",
            summary = "Change Author Password",
            description = "Change a user password from database"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
    public Response changePassword(@Valid PasswordDTO passwordDTO) {
        boolean result = authorRepository.cheangePassword(passwordDTO);
        return result ? Response.ok().build() : Response.status(500).build(); // Internal Server Error
    }

    @DELETE
    @Path("{id}")
    @Transactional
    @Operation(
            operationId = "delete",
            summary = "Delete Author",
            description = "Delete a user from database"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
    public Response delete(@PathParam("id") Long id) {
        boolean result = authorRepository.deleteById(id);
        return result ? Response.ok().build() : Response.status(500).build(); // Internal Server Error
    }
}
