package org.acme.controller;

import io.quarkus.logging.Log;
import org.acme.dto.mapper.PageMapper;
import org.acme.dto.page.PageCreateDTO;
import org.acme.dto.page.PageDTO;
import org.acme.model.Page;
import org.acme.repository.PageRepository;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("/api/v1/page")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Page APIs", description = "Article Management REST APIs")
@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.APIKEY,
        bearerFormat = "JWT"
)
public class PageController {

    @Inject
    PageRepository pageRepository;

    @Inject
    PageMapper pageMapper;

    @GET
    @Path("{uuid}")
    @Operation(
            operationId = "findByUUID",
            summary = "Get a Page",
            description = "Get an article from it's id"
    )
    @PermitAll
    public Response findByUUID(@PathParam("uuid") String uuid) {
        if (!pageRepository.existByUUID(uuid)) // Not Found
            return Response.status(404).build();
        PageDTO dto = pageMapper.toDTO(pageRepository.findByUUID(uuid));
        return Response.ok(dto).build();
    }

    @GET
    @Path("")
    @Operation(
            operationId = "findAll",
            summary = "List all Pages",
            description = "List all articles from database"
    )
    @PermitAll
    public List<PageDTO> findAll(
            @DefaultValue("0") @QueryParam("page") Integer page,
            @DefaultValue("25") @QueryParam("size") Integer size
    ) {
        return pageRepository.findAll()
                .page(page, size)
                .list()
                .stream()
                .map(pageMapper::toDTO)
                .collect(Collectors.toList());
    }

    @POST
    @Path("")
    @Transactional
    @Operation(
            operationId = "create",
            summary = "Create or Update a Page",
            description = "Create or update an article"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
    public Response create(@Valid PageCreateDTO dto) {
        try {
            Page page = pageMapper.toEntity(dto);
            pageRepository.save(page);
            return Response.ok(page).build();
        } catch (Exception e) { // Internal Server Error
            Log.error(e);
            return Response.status(500).build();
        }
    }

    @DELETE
    @Path("{uuid}")
    @Transactional
    @Operation(
            operationId = "delete",
            summary = "Delete Page",
            description = "Delete an article from database"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
    public Response delete(@PathParam("uuid") String uuid) {
        boolean result = pageRepository.deleteByUUID(uuid);
        return result ? Response.ok().build() : Response.status(404).build(); // NOT FOUND
    }
}
