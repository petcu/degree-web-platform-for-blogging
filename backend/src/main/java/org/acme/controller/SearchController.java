package org.acme.controller;

import org.acme.repository.MeiliSearchRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/v1/search")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Search APIs", description = "Search REST APIs")
public class SearchController {

    @ConfigProperty(name = "meilisearch.index.resource")
    String indexResource;

    @ConfigProperty(name = "meilisearch.index.page")
    String indexPage;

    @ConfigProperty(name = "meilisearch.index.author")
    String indexAuthor;

    @Inject
    MeiliSearchRepository meiliSearchRepository;

    @GET
    @Path("resource")
    @Operation(
            operationId = "resource",
            summary = "Get a Resource",
            description = "Get a file metadata with meilisearch index system"
    )
    public Response resource(@DefaultValue("") @QueryParam("text") String text) {
        return meiliSearchRepository.search(indexResource, text);
    }

    @GET
    @Path("page")
    @Operation(
            operationId = "page",
            summary = "Get a Page",
            description = "Get an article with meilisearch index system"
    )
    public Response page(@DefaultValue("") @QueryParam("text") String text) {
        return meiliSearchRepository.search(indexPage, text);
    }

    @GET
    @Path("author")
    @Operation(
            operationId = "author",
            summary = "Get an Author",
            description = "Get an user with meilisearch index system"
    )
    public Response author(@DefaultValue("") @QueryParam("text") String text) {
        return meiliSearchRepository.search(indexAuthor, text);
    }
}
