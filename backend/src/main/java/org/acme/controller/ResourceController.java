package org.acme.controller;

import org.acme.dto.mapper.ResourceMapper;
import org.acme.dto.resource.ResourceDTO;
import org.acme.model.form.FormFileUpload;
import org.acme.model.Resource;
import org.acme.repository.ResourceRepository;
import org.acme.service.S3Service;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.reactive.MultipartForm;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@SecurityScheme(
        scheme = "bearer",
        type = SecuritySchemeType.APIKEY,
        bearerFormat = "JWT"
)
@Path("/api/v1/resources")
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = "Resource APIs", description = "File Management REST APIs")
public class ResourceController {

    @Inject
    S3Service s3Service;

    @Inject
    ResourceRepository resourceRepository;

    @Inject
    ResourceMapper resourceMapper;

    @GET
    @Path("{uuid}")
    @Operation(
            operationId = "findByUUID",
            summary = "Get a Resource",
            description = "Get a file metadata by it's unique id"
    )
    @PermitAll
    public Response findByUUID(@PathParam("uuid") String uuid) {
        if(! resourceRepository.existByUUID(uuid)) // Not Found
            return Response.status(404).build();
        ResourceDTO dto = resourceMapper.toDTO(resourceRepository.findByUUID(uuid));
        return Response.ok(dto).build();
    }

    @GET
    @Path("")
    @Operation(
            operationId = "findAll",
            summary = "Get all Resources",
            description = "Get all files metadata filtered by page and size"
    )
    @PermitAll
    public List<ResourceDTO> findAll(
            @DefaultValue("0") @QueryParam("page") Integer page,
            @DefaultValue("25") @QueryParam("size") Integer size
    ) {
        return resourceRepository.findAll()
                .page(page, size)
                .list()
                .stream()
                .map(resourceMapper::toDTO)
                .collect(Collectors.toList());
    }

    @POST
    @Path("")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Transactional
    @Operation(
            operationId = "uploadFile",
            summary = "Upload a Resource",
            description = "Upload a file to database and storage"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
//     @PermitAll
    public Response uploadFile(@Valid @MultipartForm FormFileUpload form) {
        Resource resource = resourceRepository.save(form);
        return Objects.isNull(resource) ? Response.status(500).build() : s3Service.uploadFile(resource, form.getFile()); // Internal Server Error
    }

    @GET
    @Path("download/{objectKey}")
    @Operation(
            operationId = "downloadFile",
            summary = "Download a Resource",
            description = "Download a file from resources"
    )
    @PermitAll
    public Response downloadFile(@PathParam("objectKey") String objectKey) {
        return s3Service.downloadFile(objectKey);
    }

    @DELETE
    @Path("{uuid}")
    @Transactional
    @Operation(
            operationId = "delete",
            summary = "Delete Resource",
            description = "Delete a file from database and storage"
    )
    @RolesAllowed({"WRITER", "ADMIN"})
    public Response delete(@PathParam("uuid") String uuid) {
        if(!resourceRepository.existByUUID(uuid))
            return Response.status(404).build(); // NOT FOUND
        String objectKey = resourceRepository.findByUUID(uuid).getObjectKey();
        boolean result = resourceRepository.deleteByUUID(uuid);
        s3Service.deleteFile(objectKey);
        return result ? Response.ok().build() : Response.status(404).build(); // Internal Server Error
    }
}
