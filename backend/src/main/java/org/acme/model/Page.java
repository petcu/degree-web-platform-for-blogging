package org.acme.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page {

    @Id
    @Column(name = "uuid")
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "title")
    private String title = "";

    @Column(name = "date_update")
    private LocalDateTime date_update = LocalDateTime.now();

    @Column(name = "tags")
    private String tags = "";

    @ManyToMany
    @JoinTable(
            name = "Page_Authors",
            joinColumns = {@JoinColumn(name = "page_uuid")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")}
    )
    private Set<Author> authors;

    @Column(name = "publish")
    private Boolean publish = false;

    @Column(name = "content")
    private String content = "";
}
