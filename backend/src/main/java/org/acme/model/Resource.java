package org.acme.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Resource {
    @Id
    private String uuid = UUID.randomUUID().toString();

    @Column(name = "filename")
    public String filename;

    @Column(name = "filetype")
    public String filetype;

    @Column(name = "date_created")
    public LocalDateTime date_created = LocalDateTime.now();

    @Column(name = "filesize")
    public Long filesize;

    public String getObjectKey() {
        return String.format("%s_%s", getUuid(), filename);
    }
}
