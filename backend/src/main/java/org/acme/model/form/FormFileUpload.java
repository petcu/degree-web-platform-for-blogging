package org.acme.model.form;

import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;

import javax.validation.constraints.NotNull;
import java.io.File;

public class FormFileUpload {
    @NotNull
    @RestForm("file")
    private FileUpload file;

    public String getFileName() {
        return file.fileName();
    }

    public String getFileType() {
        return file.contentType();
    }

    public Long getFileSize() {
        return file.size();
    }

    public File getFile() {
        return file.filePath().toFile();
    }
}
