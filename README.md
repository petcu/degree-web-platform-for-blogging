# Degree - Web Platform for Blogging

## Description

In summary, a combination between frontend and backend that serve as a prototype  for the internal login that consist in a blogging app. With support for containers, of course :)

## Dependencies

- Databases:
  - PostgreSQL - for storing the data
  - MeiliSearch - as for searching

- Backend
  - Quarkus with Java
  - Hibernate
  - Smallrye OpenApi
  - Smallrye Security
  - MapStruct
  - AWS S3 Driver (used with Backblaze B2)

- Frontend
  - VueJS with Quasar (Material Components)
  - Pinia - state manager
  - Tiptap - WYSIWYG text editor with markdown suport
  - Axios - for http requests
