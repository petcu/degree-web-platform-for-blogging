const routes = [
  {
    path: "/",
    component: () => import("layouts/PublicLayout.vue"),
    children: [{ path: "", component: () => import("pages/BlogPage.vue") }],
  },
  {
    path: "/blog/:uuid",
    component: () => import("layouts/PublicLayout.vue"),
    children: [{ path: "", component: () => import("pages/ArticlePage.vue") }],
  },
  {
    path: "/login",
    component: () => import("layouts/LoginLayout.vue"),
    children: [
      { path: "", component: () => import("pages/login/LoginPage.vue") },
    ],
  },
  {
    path: "/admin",
    component: () => import("layouts/AdminLayout.vue"),
    children: [
      { path: "", component: () => import("pages/admin/AdminPage.vue") },
    ],
  },
  {
    path: "/editor",
    component: () => import("layouts/EditorLayout.vue"),
    children: [
      { path: "", component: () => import("pages/admin/EditorPage.vue") },
    ],
  },
  // 404 error
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
