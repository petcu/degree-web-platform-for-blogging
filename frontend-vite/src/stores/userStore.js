import { defineStore } from "pinia";
import { api } from "src/boot/axios";

export const useUserStore = defineStore("user", {
  state: () => ({
    data: {},
    token: "",
    isLoggenIn: false,
  }),
  getters: {
    getToken: (state) => state.token,
    getBearerToken: (state) => `Bearer ${state.token}`,
    getLoggenIn: (state) => state.isLoggenIn,
  },
  actions: {
    async getUserData(user) {
      try {
        const response = await api.get(`/api/v1/authors/username/${user}`);
        this.data = await response.data;
      } catch (error) {
        console.log(error);
      }
    },
    async logIn(user, pass) {
      this.logOut();
      try {
        const response = await api.post("/api/v1/auth", {
          username: user,
          password: pass,
        });
        this.token = await response.data;
        this.isLoggenIn = this.token.length > 1;
        api.defaults.headers.common.Authorization = this.getBearerToken;
      } catch (error) {
        console.log(error);
      }
    },
    logOut() {
      this.data = {};
      this.token = "";
      this.isLoggenIn = false;
      api.defaults.headers.common.Authorization = "";
    },
  },
});
