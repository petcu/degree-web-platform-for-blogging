import { defineStore } from "pinia";
import { api } from "src/boot/axios";
import { useUserStore } from "./userStore";

const userStore = useUserStore();

export const useArticleStore = defineStore("article", {
  state: () => ({
    uuid: "",
    title: "",
    tags: [],
    content: "",
    publish: false,
    // authors: [],
  }),
  getters: {
    isLoaded: (state) => state.uuid !== undefined && state.uuid.length > 0,
  },
  actions: {
    async update() {
      var objectData = {
        title: this.title,
        tags: this.tags,
        authors: [userStore.data.uuid],
        content: this.content,
        publish: this.publish,
      };
      if (this.uuid.length > 0) objectData["uuid"] = this.uuid;
      api
        .post(`/api/v1/page`, objectData, {
          headers: {
            Authorization: userStore.getBearerToken,
          },
        })
        .then((response) => {
          console.log("Article update");
          console.log(response.data);
          this.uuid = response.data.uuid;
        })
        .catch((error) => {
          console.log(error);
        });
    },
    async load() {
      try {
        const response = await api.get(`/api/v1/page/${this.uuid}`);
        const data = await response.data;
        this.uuid = data.uuid;
        this.title = data.title;
        this.tags = data.tags;
        this.content = data.content;
        this.publish = data.publish;
      } catch (error) {
        console.log(error);
      }
    },
    newArticle() {
      this.uuid = "";
      this.title = "";
      this.tags = [];
      this.content = "";
      this.publish = false;
      // this.authors = [];
    },
  },
});
