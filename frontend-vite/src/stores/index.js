import { store } from "quasar/wrappers";
import { createPinia } from "pinia";
import { LocalStorage } from "quasar";
import { watch } from "vue";

export default store((/* { ssrContext } */) => {
  const pinia = createPinia();
  const key = "pinia-key";

  if (LocalStorage.getItem(key)) pinia.state.value = LocalStorage.getItem(key);

  watch(
    pinia.state,
    (state) => {
      LocalStorage.set(key, state);
    },
    { deep: true }
  );

  return pinia;
});
