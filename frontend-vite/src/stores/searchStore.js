import { defineStore } from "pinia";
import { api } from "src/boot/axios";

export const useSearchStore = defineStore("search", {
  state: () => ({
    data: {},
  }),
  getters: {},
  actions: {
    async fetchPosts(text) {
      try {
        const response = await api.get(`/api/v1/search/page?text=${text}`);
        const data = await response.data;
        this.data = data.filter((x) => x.publish == true);
        console.log(this.data);
      } catch (error) {
        console.log(error);
      }
    },
  },
});
